# Leap-skills-form

- Technology stacks:
	1. Node.js
	2. Express.js
	3. MongoDB

- How to start node server.
	1. Move inside the root directory of project and run the following commands:
		- 'npm install' (It will install all node.js dependencies)
		- 'node server.js' (it will start the node.js server on port 8080)
		
- You can change the server port by modifying the server.js file.
- Please Make sure MongoDB is running properly on default port 27017 or you can change the port no. by modifying the ./models/DBConnection.js file.


- APIs: 
	1. POST: '/signup': Create a new user
		Request body: {"u_name": "subhash", "p_no": "1236566660", "e_id": "subhash.negi@gmail.com","u_p": "subhash1234"}
	2. POST: '/login': Start the session
		Request body: {"e_id": "subhash.negi@tercept.com","u_p": "subhash1234"}
	3. GET: '/logout': End the session
	4. POST: '/note': create a new note
		Request body: {"content": "\n Hi All \n","name": "text2.txt"}
	5. POST: '/fnote': fetch a note
		Request body: {"name": "text2.txt"}
	6. DELETE: '/delete': Delete a note
		Request body: {"name": "text2.txt"}
	7. GET: '/fetch_notes': Fetch all the notes

- Note: Without the login session you can not access the following apis. You have to pass session token in cookie of each request.
	'/note', '/fnote', '/delete', '/fetch_notes'
