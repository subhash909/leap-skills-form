var db_connection_model = require("./DBConnection");
var logging_model = require('./LoggingModel');
var auth_model = require('./AuthModel');

var db_name = "loginform";
var collection_name = "users";

var signup = function(req, res){
    db_connection_model.get_db_connection(function(err, client){
        if(err){
            logging_model.log_server_error("DB", "Error occurred in getting DB connection", err);
            client.close();
            res.send({"STATUS": "ERROR", "TYPE": "SERVER_ERROR"});
        }else{
            var user_name = req.body.u_name;
            var phone_no = req.body.p_no;
            var email_id = req.body.e_id;
            var user_pass = req.body.u_p;
            auth_model.crypt_password(user_pass, (err, pass_hash)=>{
                if(err) logging_model.log_server_error("DB", "Error occurred in crypt_password", err);
                else{
                    user_pass = pass_hash;
                    var entry_obj = {user_name: user_name, phone_no: phone_no, email_id: email_id, user_pass: user_pass};
                    check_duplicate_entry(client, entry_obj, res);
                }
            });
        }
    });
}

function check_duplicate_entry(client, entry_obj, res){
    client.db(db_name).collection(collection_name).find({email_id: entry_obj.email_id}).toArray(function(err, result){
        if(err){
            logging_model.log_server_error("DB", "Error occurred in checking duplicate entry", err);
            client.close();
            res.send({"STATUS": "ERROR", "TYPE": "SERVER_ERROR"});
        }else if(result.length>0){
            client.close();
            res.send({"STATUS": "ERROR", "TYPE": "DUPLICATE"});
        }else{
            insert_entry(client, entry_obj, res);
        }
    });
}

function insert_entry(client, entry_obj, res){
    client.db(db_name).collection(collection_name).insert(entry_obj, function(err, result){
        client.close();
        if(err){
            logging_model.log_server_error("DB", "Error occurred in inserting data in DB", err);
            res.send({"STATUS": "ERROR", "TYPE": "SERVER_ERROR"});
        }else{
            res.send({"STATUS": "SUCCESS"});
            logging_model.log_server_info("DB", "New entry inserted into DB");
        }
    });
}

var export_obj = {};
export_obj.signup = signup;

module.exports = export_obj;