var signup_model = require('./SignUpModel');
var aut_model = require('./AuthModel');
var file_upload_model = require('./NoteModel');
var path = require('path');

var register = function(server){

    server.route('/signup')
    .post(signup_model.signup);

    server.route('/login')
    .post(aut_model.login);

    server.route('/logout')
    .get(aut_model.logout);

    //  Session validation middleware
    server.use((req, res, next)=>{
        aut_model.check_token(req, res, function(req, res, u_email){
            if(u_email){
                req.body.u_s_email = u_email;
                next();
            }else{
                res.status(401).end();
            }
        });
    });

    server.route('/note')
    .post(file_upload_model.create_note);

    server.route('/fetch_notes')
    .get(file_upload_model.read_all_notes);

    server.route('/fnote')
    .post(file_upload_model.fetch_note);

    server.route('/delete')
    .delete(file_upload_model.delete_note);

}

var export_obj = {};
export_obj.register = register;

module.exports = export_obj;