var fs = require('fs');

if (!fs.existsSync('./logs')){
    fs.mkdirSync('./logs');
}

var log_server_info = function(type, message){
    fs.appendFile("./logs/server_info_log.log", `\n${type}\t${message}`, function(err){
        if(err) console.log("Error occurred in writing log in log_server_info: ", err);
    });
}

var log_server_error = function(type, message, err){
    fs.appendFile("./logs/server_error_log.log", `\n${type}\t${message}\t${err}`, function(err){
        if(err) console.log("Error occurred in writing log in log_server_error: ", err);
    });
}

var export_obj = {};
export_obj.log_server_info = log_server_info;
export_obj.log_server_error = log_server_error;

module.exports = export_obj;