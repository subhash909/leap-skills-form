var fs = require('fs');

var logging_model = require('./LoggingModel');

var parent_dir = './tmp'

var create_user_directory = function(dir_name){
    if (!fs.existsSync(`${parent_dir}`)){
        fs.mkdirSync(`${parent_dir}`);
    }
    if (!fs.existsSync(`${parent_dir}/${dir_name}`)){
        fs.mkdirSync(`${parent_dir}/${dir_name}`);
    }
}

var create_note = function(req, res){
    var data = req.body || {};
    var note_content = data.content || "";
    var name = data.name || "";
    var email = data.u_s_email || "";
    if(!name || !note_content || !email){
        res.status(422).end();
        return;
    }
    create_user_directory(email);
    fs.writeFile(`${parent_dir}/${email}/${name}`, note_content, (err)=>{
        if(err){
            logging_model.log_server_error("NOTE", "Error occurred in create_note", err);
            res.status(422).end();
        }else{
            res.status(200).end();
        }
    });
}

var delete_note = function(req, res){
    var data = req.body || {};
    var email = data.u_s_email || "";
    var note_name = data.name || "";
    if(!note_name){
        res.status(422).end();
    }
    fs.unlink(`${parent_dir}/${email}/${note_name}`, (err)=>{
        if(err){
            logging_model.log_server_error("NOTE", "Error occurred in create_note", err);
            res.status(422).end();
        }else{
            res.status(200).end();
        }
    });
}

var fetch_note = function(req, res){
    var data = req.body || {};
    var email = data.u_s_email || "";
    var note_name = data.name || "";
    if(!note_name){
        res.status(422).end();
    }
    fs.readFile(`${parent_dir}/${email}/${note_name}`, 'utf8', (err, result)=>{
        if(err){
            logging_model.log_server_error("NOTE", "Error occurred in create_note", err);
            res.status(422).end();
        }else{
            res.status(200).send({"content": result}).end();
        }
    });
}

var read_all_notes = function(req, res){
    var data = req.body || {};
    var email = data.u_s_email || "";
    create_user_directory(email);
    fs.readdir(`${parent_dir}/${email}`, (err, result)=>{
        if(err){
            logging_model.log_server_error("NOTE", "Error occurred in create_note", err);
            res.status(422).end();
        }else{
            res.send(result).end();
        }
    });
}

var export_obj = {};
export_obj.create_note = create_note;
export_obj.read_all_notes = read_all_notes;
export_obj.delete_note = delete_note;
export_obj.fetch_note = fetch_note;

module.exports = export_obj;