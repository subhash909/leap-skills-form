var db_connection_model = require("./DBConnection");
var logging_model = require('./LoggingModel');
var bcrypt = require('bcrypt');
var jwt = require('jsonwebtoken');

var jwt_secret = "subhash.negi.908@gmail.com";
var all_tokens = {};
var db_name = "loginform";
var collection_name = "users";

crypt_password = function(password, callback) {
    bcrypt.genSalt(10, function(err, salt) {
        if (err) return callback(err);
        bcrypt.hash(password, salt, function(err, hash) {
            return callback(err, hash);
        });
    });
};

compare_password = function(plainPass, hashword, callback) {
    bcrypt.compare(plainPass, hashword, function(err, isPasswordMatch) {   
        return err == null ? callback(null, isPasswordMatch) : callback(err);
    });
};

function check_login(req, res){
    db_connection_model.get_db_connection(function(err, client){
        if(err){
            logging_model.log_server_error("DB", "Error occurred in getting DB connection", err);
            client && client.close();
            res.send({"STATUS": "ERROR", "TYPE": "SERVER_ERROR"});
        }else{
            var email_id = req.body.e_id;
            var user_pass = req.body.u_p;
            get_user_details(client, email_id, function(err, entry_result){
                client.close();
                if(entry_result && entry_result.length==1){
                    compare_password(user_pass, entry_result[0].user_pass, function(err, is_valid){
                        if(is_valid){
                            var token = jwt.sign({data: {email_id: email_id}}, jwt_secret,{ expiresIn: 60*60*3 });
                            all_tokens[token]=true;
                            res.cookie('t_token', token);
                            res.send({"STATUS": "SUCCESS", "t_token": token});
                        }else{
                            if(err) logging_model.log_server_error("DB", "Error occurred in compare_password", err);
                            res.send({"STATUS": "ERROR", "TYPE": "INVALID"});
                        }
                    });
                }else{
                    if(err) logging_model.log_server_error("DB", "Error occurred in get_user_details", err);
                    res.send({"STATUS": "ERROR", "TYPE": "INVALID"});
                }
            });
        }
    });
}

var logout = function(req, res){
    var token = getCookie(req.headers.cookie,'t_token');
    all_tokens[token] = false;
    res.cookie('t_token', "");
    res.send({"STATUS": "SUCCESS"});
}

var check_token = function(req, res, redir_method){
    var token = getCookie(req.headers.cookie,'t_token');
    if(token){
        jwt.verify(token,  jwt_secret, function(err, decoded) {
            if(all_tokens[token] && decoded && decoded.data  && decoded.data.email_id && redir_method){
                redir_method(req, res, decoded.data.email_id);
            }else{
                res.cookie('t_token', "");
                res.send({"session": false});
            }
        });
    }else{
        redir_method(req, res);
    }
}

function get_user_details(client, email_id, callback){
    client.db(db_name).collection(collection_name).find({email_id: email_id}).toArray(callback);
}

function getCookie(cookie,cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

var export_obj = {};
export_obj.login = check_login;
export_obj.check_token = check_token;
export_obj.crypt_password = crypt_password;
export_obj.logout = logout;

module.exports = export_obj;
